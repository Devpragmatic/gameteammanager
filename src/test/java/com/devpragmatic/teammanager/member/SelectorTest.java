package com.devpragmatic.teammanager.member;

import com.devpragmatic.teammanager.member.config.MemberPortsConfig;
import com.devpragmatic.teammanager.member.ports.output.dto.MemberState;
import com.devpragmatic.teammanager.member.ports.impl.MemberRepository;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {MemberConfig.class, MemberPortsConfig.class})
public class SelectorTest {

    private static final String NAME = "name";
    private static final String SURNAME = "surname";
    private static final String NICK = "nick";
    private static final int TWO_CREATED_MEMBERS = 2;

    @Autowired
    private Selector selector;
    @Autowired
    private MemberRepository memberRepository;

    @After
    public void clear(){
        memberRepository.clear();
    }

    @Test
    public void shouldBeABean() {
        // given
        // when
        // then
        assertNotNull(selector);
    }

    @Test
    public void canSaveNewMemberAndEstablishUniqueId() {
        // given
        // when
        Member member = selector.addMember(NAME, SURNAME, NICK);
        // then
        assertNotNull(member);
        assertNotNull(member.getId());
    }

    @Test
    public void memberCreateShouldBeSignalizedForOutputPorts(){
        // given
        // when
        Member member = selector.addMember(NAME, SURNAME, NICK);
        // then
        assertCorrectState(memberRepository.get(member.getId()));
    }

    @Test
    public void shouldCreateTwoMembersWithTheSameNameSurnameAndNick(){
        // given
        selector.addMember(NAME, SURNAME, NICK);
        // when
        selector.addMember(NAME, SURNAME, NICK);
        // then
        assertEquals(TWO_CREATED_MEMBERS, memberRepository.size());
    }


    @Test
    public void canGetAMemberByUniqueId(){
        // given
        Member expectedMemberOne = selector.addMember(NAME, SURNAME, NICK);
        Member expectedMemberTwo = selector.addMember(NAME, SURNAME, NICK);
        // when
        Member memberOne = selector.getMember(expectedMemberOne.getId());
        Member memberTwo = selector.getMember(expectedMemberTwo.getId());
        // then
        assertEquals(expectedMemberOne, memberOne);
        assertCorrectState(memberOne.getState());
        assertEquals(expectedMemberTwo, memberTwo);
        assertCorrectState(memberTwo.getState());
    }

    private void assertCorrectState(MemberState actualState) {
        assertNotNull(actualState);
        assertEquals(NAME, actualState.getName());
        assertEquals(SURNAME, actualState.getSurname());
        assertEquals(NICK, actualState.getNick());
        assertNull(actualState.getTeam());
    }
}
