package com.devpragmatic.teammanager.member.config;

import com.devpragmatic.teammanager.member.ports.input.IdGenerator;
import com.devpragmatic.teammanager.member.ports.input.impl.IdGeneratorImpl;
import com.devpragmatic.teammanager.member.ports.impl.MemberRepository;
import com.devpragmatic.teammanager.member.ports.output.MemberCreateListener;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;

@TestConfiguration
public class MemberPortsConfig {

    @Bean
    MemberCreateListener memberRepositoryImpl(){
        return new MemberRepository();
    }

    @Bean
    IdGenerator idGenerator() {
        return new IdGeneratorImpl();
    }
}
