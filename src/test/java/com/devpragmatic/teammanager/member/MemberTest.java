package com.devpragmatic.teammanager.member;

import com.devpragmatic.teammanager.member.ports.input.Team;
import com.devpragmatic.teammanager.member.ports.input.impl.TeamImpl;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class MemberTest {

    private static final Long ID = 1L;
    private static final String NAME = "name";
    private static final String SURNAME = "surname";
    private static final String NICK = "nick";

    private Member member;

    @Before
    public void createMember(){
        member = new Member(ID,NAME, SURNAME, NICK);
    }

    @Test
    public void canIntroduceYourself(){
        // when
        member.introduceYourself();
        // then
        assertEquals(NAME + "'" + NICK + "'" + SURNAME, member.introduceYourself());
    }

    @Test
    public void canChangeItsPersonalData() {
        // given
        String newName = "new Name";
        String newSurname = "new Surname";
        String newNick = "newNick";
        // when
        member.changePersonalDataTo(newName, newSurname, newNick);
        // then
        assertEquals(newName + "'" + newNick + "'" + newSurname, member.introduceYourself());
    }

    @Test
    public void canJoinToTeam() {
        // given
        Team team = new TeamImpl();
        // when
        member.joinToTeam(team);
        // then
        assertEquals(team.getName(), member.tellTheNameOfYourTeam());
    }

    @Test
    public void canShowId() {
        // given
        // when
        Long id = member.getId();
        // then
        assertEquals(ID, id);
    }

    @Test
    public void membersAreEqualsWhenIdIsTheSame(){
        // given
        String unUsed = "unUsed";
        Member memberTwo = new Member(ID, unUsed, unUsed, unUsed);
        // when
        boolean equals = member.equals(memberTwo);
        int memberHashCode = member.hashCode();
        int memberTwoHashCode = memberTwo.hashCode();
        // then
        assertTrue(equals);
        assertEquals(memberHashCode, memberTwoHashCode);
    }


    @Test
    public void membersAreNotEqualsWhenIdIsDifferent(){
        // given
        Member memberTwo = new Member(ID + 1, NAME, SURNAME, NICK);
        // when
        boolean equals = member.equals(memberTwo);
        int memberHashCode = member.hashCode();
        int memberTwoHashCode = memberTwo.hashCode();
        // then
        assertFalse(equals);
        assertNotEquals(memberHashCode, memberTwoHashCode);
    }
}
