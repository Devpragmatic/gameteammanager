package com.devpragmatic.teammanager.member.ports.impl;

import com.devpragmatic.teammanager.member.ports.input.MemberSupplier;
import com.devpragmatic.teammanager.member.ports.output.MemberCreateListener;
import com.devpragmatic.teammanager.member.ports.output.dto.MemberState;
import org.springframework.stereotype.Repository;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Repository
public class MemberRepository implements MemberCreateListener, MemberSupplier {

    private Map<Long, MemberState> inMemoryDB = new ConcurrentHashMap<>();

    @Override
    public void apply(long id, MemberState memberState) {
        inMemoryDB.put(id, memberState);
    }

    public MemberState get(long id) {
        return inMemoryDB.get(id);
    }

    public int size(){
        return inMemoryDB.size();
    }

    public void clear(){
        inMemoryDB.clear();
    }
}
