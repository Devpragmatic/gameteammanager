package com.devpragmatic.teammanager.member.ports.input.impl;

import com.devpragmatic.teammanager.member.ports.input.IdGenerator;
import org.springframework.stereotype.Service;

import java.util.concurrent.atomic.AtomicLong;

@Service
public class IdGeneratorImpl implements IdGenerator {

    private final AtomicLong atomicLong = new AtomicLong();

    @Override
    public long generate() {
        return atomicLong.getAndIncrement();
    }
}
