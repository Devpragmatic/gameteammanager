package com.devpragmatic.teammanager.member.ports.input.impl;

import com.devpragmatic.teammanager.member.ports.input.Team;

public class TeamImpl implements Team {

    @Override
    public String getName() {
        return "TestTeam";
    }
}
