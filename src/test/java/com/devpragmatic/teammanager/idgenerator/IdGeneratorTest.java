package com.devpragmatic.teammanager.idgenerator;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = IdGeneratorConfig.class)
public class IdGeneratorTest {

    @Autowired
    private IdGenerator idGenerator;

    @Test
    public void shouldBeABean() {
        // given
        // when
        // then
        assertNotNull(idGenerator);
    }
}
