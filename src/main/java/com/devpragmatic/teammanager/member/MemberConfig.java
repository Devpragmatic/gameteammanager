package com.devpragmatic.teammanager.member;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MemberConfig {

    @Bean
    public Selector memberFactory(){
        return new Selector();
    }
}
