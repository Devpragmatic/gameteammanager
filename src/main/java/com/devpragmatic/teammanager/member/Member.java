package com.devpragmatic.teammanager.member;

import com.devpragmatic.teammanager.member.ports.input.Team;
import com.devpragmatic.teammanager.member.ports.output.dto.MemberState;

public class Member {

    private long id;
    private String name;
    private String surname;
    private String nick;
    private String team;

    Member(long id, String name, String surname, String nick) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.nick = nick;
    }

    public Member(long id, MemberState memberState) {
        this.id = id;
        this.name = memberState.getName();
        this.surname = memberState.getSurname();
        this.nick = memberState.getNick();
    }

    public void changePersonalDataTo(String newName, String newSurname, String newNick) {
        this.name = newName;
        this.surname = newSurname;
        this.nick = newNick;
    }

    public String introduceYourself() {
        return name + "'" + nick + "'" + surname;
    }

    public void joinToTeam(Team team) {
        this.team = team.getName();
    }

    public String tellTheNameOfYourTeam() {
        return team;
    }

    Long getId() {
        return id;
    }

    MemberState getState(){
        return new MemberState(name, surname, nick, team);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Member)) return false;

        Member member = (Member) o;

        return id == member.id;
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }
}
