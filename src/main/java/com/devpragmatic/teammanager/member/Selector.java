package com.devpragmatic.teammanager.member;

import com.devpragmatic.teammanager.member.ports.input.IdGenerator;
import com.devpragmatic.teammanager.member.ports.input.MemberSupplier;
import com.devpragmatic.teammanager.member.ports.output.MemberCreateListener;
import com.devpragmatic.teammanager.member.ports.output.dto.MemberState;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class Selector {

    @Autowired
    public IdGenerator idGenerator;
    @Autowired
    public List<MemberCreateListener> memberCreateListeners;
    @Autowired
    public MemberSupplier memberSupplier;

    public Member addMember(String name, String surname, String nick) {
        long id = idGenerator.generate();
        Member member = new Member(id, name, surname, nick);
        MemberState state = member.getState();
        memberCreateListeners.forEach(listener -> listener.apply(id, state));
        return member;
    }

    public Member getMember(long id){
        MemberState memberState = memberSupplier.get(id);
        return new Member(id, memberState);
    }
}
