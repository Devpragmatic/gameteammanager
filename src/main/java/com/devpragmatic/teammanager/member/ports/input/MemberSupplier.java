package com.devpragmatic.teammanager.member.ports.input;

import com.devpragmatic.teammanager.member.ports.output.dto.MemberState;

public interface MemberSupplier {
    MemberState get(long id);
}
