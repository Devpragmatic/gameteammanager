package com.devpragmatic.teammanager.member.ports.input;

public interface IdGenerator {

    long generate();
}
