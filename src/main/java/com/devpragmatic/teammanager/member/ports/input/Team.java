package com.devpragmatic.teammanager.member.ports.input;

public interface Team {

    String getName();
}
