package com.devpragmatic.teammanager.member.ports.output.dto;

public class MemberState {

    private String name;
    private String surname;
    private String nick;
    private String team;


    public MemberState(String name, String surname, String nick, String team) {
        this.name = name;
        this.surname = surname;
        this.nick = nick;
        this.team = team;
    }

    public String getName() {
        return name;
    }
    public String getSurname() {
        return surname;
    }

    public String getNick() {
        return nick;
    }

    public String getTeam() {
        return team;
    }
}
