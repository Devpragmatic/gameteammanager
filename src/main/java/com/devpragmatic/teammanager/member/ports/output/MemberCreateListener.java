package com.devpragmatic.teammanager.member.ports.output;

import com.devpragmatic.teammanager.member.ports.output.dto.MemberState;

@FunctionalInterface
public interface MemberCreateListener {

    void apply(long id, MemberState memberState);
}
