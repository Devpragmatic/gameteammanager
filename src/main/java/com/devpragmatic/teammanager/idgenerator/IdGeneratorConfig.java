package com.devpragmatic.teammanager.idgenerator;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class IdGeneratorConfig {

    @Bean
    public IdGenerator idGenerator(){
        return new IdGeneratorImpl();
    }
}
